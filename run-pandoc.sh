#!/bin/sh
pandoc 'index.md' -o 'output/document.pdf' --from markdown --template './theme/eisvogel.tex' --listings --toc
