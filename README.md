# Pandoc Report Template

Convert markdown into PDF with an awesome LaTeX template. 

## Example

You can find a document example at [output/document.pdf](output/document.pdf).

## Usage

Clone the repository with `git clone` :

```bash
$ git clone https://gitlab.com/xanhacks/pandoc-report-template
$ cd pandoc-report-template
```

Then, you can edit your document content in `index.md`.

Finally, you can render your document into a PDF file, `output/document.pdf`.

```bash
$ bash build.sh
```

## Reference

- Pandoc theme : [github.com/Wandmalfarbe/pandoc-latex-template/](https://github.com/Wandmalfarbe/pandoc-latex-template/) 
- Docker image : [github.com/tewarid/docker-pandoc](https://github.com/tewarid/docker-pandoc/).

